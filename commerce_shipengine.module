<?php

use Drupal\Core\Form\FormStateInterface;

/**
 * Use shipengine-fulfillment template.
 *
 * Implements hook_theme_registry_alter().
 */
function commerce_shipengine_theme_registry_alter(&$theme_registry) {
  $theme_registry['commerce_order__admin']['template'] = 'shipengine-fulfillment';
  $theme_registry['commerce_order__admin']['path'] = 'modules/custom/commerce_shipengine/templates';
}

/**
 * Add shipping labels to order view.
 */
function commerce_shipengine_preprocess_commerce_order__admin(&$variables) {
  $shipments = $variables['order_entity']->get('shipments')->referencedEntities();
  $labels = [];
  foreach ($shipments as $shipment) {
    if ($label = $shipment->getData('label')) {
      $labels[] = $label;
    }
  }
  $variables['order']['shipping_labels'] = $labels;
}

/**
 * Add submit handler to shipment edit form.
 */
function commerce_shipengine_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id  === 'commerce_shipment_default_edit_form') {
    array_unshift($form['actions']['submit']['#submit'], '_commerce_shipengine_recreate_labels');
  }
}

/**
 * Recreate shipengine labels.
 */
function _commerce_shipengine_recreate_labels(&$form, FormStateInterface $form_state) {
  $shipment = $form_state->getBuildInfo()['callback_object']->getEntity();
  $shipengine_void_request = \Drupal::service('commerce_shipengine.void_request');
  $shipengine_void_request->setShipment($shipment);
  $config = $shipment->getShippingMethod()->getPlugin()->getConfiguration();
  $shipengine_void_request->setConfig($config);
  $label = $shipment->getData('label');

  if ($label) {
    $shipengine_void_request->voidLabel($label['label_id']);
  }

  $shipengine_label_request = \Drupal::service('commerce_shipengine.label_request');
  $shipengine_label_request->setShipment($shipment);
  $shipengine_label_request->setConfig($config);
  $label = $shipengine_label_request->createLabel();
  $shipment->setData('label', $label);
}
